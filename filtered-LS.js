var fs = require('fs');

var dir = process.argv[2];
var ext = process.argv[3];


fs.readdir(dir, doneReading)

function doneReading(error, content)
{
    var lines = content.toString().split(',');
    
    for(var i = 0; i < lines.length; i++)
    {
        if(lines[i].split('.')[1] === ext)
        {
            console.log(lines[i]);
        }
    }
}

/*
var fs = require('fs')
var path = require('path')

var folder = process.argv[2]
var ext = '.' + process.argv[3]

fs.readdir(folder, function (err, files) {
  if (err) return console.error(err)
  files.forEach(function (file) {
    if (path.extname(file) === ext) {
      console.log(file)
    }
  })
})
*/