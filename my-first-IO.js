var fs = require('fs');

var content = fs.readFileSync(process.argv[2]).toString();

var sub = content.split('\n');

console.log(sub.length - 1);

/*
    var fs = require('fs')

    var contents = fs.readFileSync(process.argv[2])
    var lines = contents.toString().split('\n').length - 1
    console.log(lines)

    // note you can avoid the .toString() by passing 'utf8' as the
*/